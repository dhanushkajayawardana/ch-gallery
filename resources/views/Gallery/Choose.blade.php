<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="summernote">@lang('blocks.block.creation.image')</label>
      <div>
        <a href="javascript:void(0);" data-toggle="modal" data-target="#ChooseGallery"
           class="btn btn-secondary btn-block-sm">@lang('blocks.block.creation.chooseImage')</a>
      </div>
      <figure class="mt-2 grid-item @if($image === null)hidden @endif" id="ChoosenThumnail" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img class="img-thumbnail img-fluid" src="{{ Gallery::thumbnail($image, 200) }}" itemprop="thumbnail" height="100%">
          <span class="RemoveChoosen"><i class="fa fa-times"></i></span>
      </figure>
      <input type="hidden" id="ChoosenImage" name="image" value="{{ $image }}">
      <div class="modal fade text-left" id="ChooseGallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16"
           style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel16">@lang('gallery.ChooseModal.heading')</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row masonry-grid my-gallery">
                @if(count($images))
                  @foreach($images as $image)
                    <figure class="col-lg-2 col-md-3 col-12 grid-item" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                      <a href="javascript:void(0);" data-image="{{Storage::url($image->path)}}" class="chooseImage" itemprop="contentUrl" data-size="800x360">
                        <img class="img-thumbnail img-fluid" src="{{ Gallery::thumbnail($image->path, 200) }}" itemprop="thumbnail" height="100%">
                        <span>{{$image->title}}</span>
                      </a>
                    </figure>
                  @endforeach
                @else
                  <div class="col-lg-3 col-md-12">
                    <div href="{{ route('admin.CreateFolder') }}" class="card blocks-folder">
                      <div class="card-content">
                        <div class="card-body text-center">
                          <h4 class="card-title">@lang('gallery.noImages')</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>